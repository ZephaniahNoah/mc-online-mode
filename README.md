# About
At some point Mojang changed the URL for authentication which broke online mode for older versions of Minecraft. This is a small mod that fixes online mode.

# Compile
* This is the command to compile the class file.
* `javac /path/to/go.java -classpath /path/to/minecraft-alpha-1.0.16.jar`
* Only java 8 was tested. Newer version of java probably won't work.

# Versions
Here is a list of class files alongside their corresponding Minecraft versions.
* go.java - a1.0.16
* gx.java - a1.0.17_04
* gy.java - a1.1.0
* ht.java - a1.2.0
