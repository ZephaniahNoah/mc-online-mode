import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Random;
import net.minecraft.client.Minecraft;

// 
// Decompiled by Procyon v0.5.36
// 

public class go extends kq
{
    private boolean c;
    private hy d;
    public String a;
    private Minecraft e;
    private gj f;
    private boolean g;
    Random b;
    
    public go(final Minecraft e, final String host, final int port) {
        this.c = false;
        this.g = false;
        this.b = new Random();
        this.e = e;
        try {
            this.d = new hy(new Socket(InetAddress.getByName(host), port), "Client", this);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void a() {
        if (this.c) {
            return;
        }
        this.d.a();
    }
    
    @Override
    public void a(final hf hf) {
        this.e.b = new mw(this.e, this);
        this.f = new gj(this);
        this.f.u = true;
        this.e.a(this.f);
        this.e.a(new db(this));
    }
    
    @Override
    public void a(final gq gq) {
        final dr dr = new dr(this.f, gq.b / 32.0, gq.c / 32.0, gq.d / 32.0, new eo(gq.h, gq.i));
        dr.ak = gq.e / 128.0;
        dr.al = gq.f / 128.0;
        dr.am = gq.g / 128.0;
        dr.ba = gq.b;
        dr.bb = gq.c;
        dr.bc = gq.d;
        this.f.a(gq.a, dr);
    }
    
    @Override
    public void a(final jz jz) {
        final double n = jz.b / 32.0;
        final double n2 = jz.c / 32.0;
        final double n3 = jz.d / 32.0;
        jx jx = null;
        if (jz.e == 10) {
            jx = new no(this.f, n, n2, n3, 0);
        }
        if (jz.e == 11) {
            jx = new no(this.f, n, n2, n3, 1);
        }
        if (jz.e == 12) {
            jx = new no(this.f, n, n2, n3, 2);
        }
        if (jz.e == 1) {
            jx = new cx(this.f, n, n2, n3);
        }
        if (jx != null) {
            jx.ba = jz.b;
            jx.bb = jz.c;
            jx.bc = jz.d;
            jx.an = 0.0f;
            jx.ao = 0.0f;
            jx.Z = jz.a;
            this.f.a(jz.a, jx);
        }
    }
    
    @Override
    public void a(final gg gg) {
        final double n = gg.c / 32.0;
        final double n2 = gg.d / 32.0;
        final double n3 = gg.e / 32.0;
        final float n4 = gg.f * 360 / 256.0f;
        final float n5 = gg.g * 360 / 256.0f;
        final ng ng = new ng(this.e.e, gg.b);
        ng.ba = gg.c;
        ng.bb = gg.d;
        ng.bc = gg.e;
        final int h = gg.h;
        if (h == 0) {
            ng.b.a[ng.b.d] = null;
        }
        else {
            ng.b.a[ng.b.d] = new eo(h);
        }
        ng.b(n, n2, n3, n4, n5);
        this.f.a(gg.a, ng);
    }
    
    @Override
    public void a(final jb jb) {
        final jx b = this.f.b(jb.a);
        if (b == null) {
            return;
        }
        b.ba = jb.b;
        b.bb = jb.c;
        b.bc = jb.d;
        b.a(b.ba / 32.0, b.bb / 32.0, b.bc / 32.0, jb.e * 360 / 256.0f, jb.f * 360 / 256.0f, 3);
    }
    
    @Override
    public void a(final lf lf) {
        final jx b = this.f.b(lf.a);
        if (b == null) {
            return;
        }
        final jx jx = b;
        jx.ba += lf.b;
        final jx jx2 = b;
        jx2.bb += lf.c;
        final jx jx3 = b;
        jx3.bc += lf.d;
        b.a(b.ba / 32.0, b.bb / 32.0, b.bc / 32.0, lf.g ? (lf.e * 360 / 256.0f) : b.an, lf.g ? (lf.f * 360 / 256.0f) : b.ao, 3);
    }
    
    @Override
    public void a(final jj jj) {
        this.f.c(jj.a);
    }
    
    @Override
    public void a(final eb eb) {
        final be g = this.e.g;
        double n = g.ah;
        double n2 = g.ai;
        double n3 = g.aj;
        float n4 = g.an;
        float n5 = g.ao;
        if (eb.h) {
            n = eb.a;
            n2 = eb.b;
            n3 = eb.c;
        }
        if (eb.i) {
            n4 = eb.e;
            n5 = eb.f;
        }
        g.aI = 0.0f;
        final be be = g;
        final be be2 = g;
        final be be3 = g;
        final double ak = 0.0;
        be3.am = ak;
        be2.al = ak;
        be.ak = ak;
        g.b(n, n2, n3, n4, n5);
        eb.a = g.ah;
        eb.b = g.ar.b;
        eb.c = g.aj;
        eb.d = g.ai;
        this.d.a(eb);
        if (!this.g) {
            this.e.g.ae = this.e.g.ah;
            this.e.g.af = this.e.g.ai;
            this.e.g.ag = this.e.g.aj;
            this.g = true;
            this.e.a((bd)null);
        }
    }
    
    @Override
    public void a(final jq jq) {
        this.f.a(jq.a, jq.b, jq.c);
    }
    
    @Override
    public void a(final mn mn) {
        final fr b = this.f.b(mn.a, mn.b);
        final int n = mn.a * 16;
        final int n2 = mn.b * 16;
        for (int i = 0; i < mn.f; ++i) {
            final short n3 = mn.c[i];
            final int n4 = mn.d[i] & 0xFF;
            final byte b2 = mn.e[i];
            final int n5 = n3 >> 12 & 0xF;
            final int n6 = n3 >> 8 & 0xF;
            final int n7 = n3 & 0xFF;
            b.a(n5, n7, n6, n4, b2);
            this.f.c(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
            this.f.b(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
        }
    }
    
    @Override
    public void a(final bv bv) {
        this.f.c(bv.a, bv.b, bv.c, bv.a + bv.d - 1, bv.b + bv.e - 1, bv.c + bv.f - 1);
        this.f.a(bv.a, bv.b, bv.c, bv.d, bv.e, bv.f, bv.g);
    }
    
    @Override
    public void a(final kx kx) {
        this.f.c(kx.a, kx.b, kx.c, kx.d, kx.e);
    }
    
    @Override
    public void a(final nt nt) {
        this.d.a("Got kicked");
        this.c = true;
        this.e.a((cj)null);
        this.e.a(new cf("Disconnected by server", nt.a));
    }
    
    @Override
    public void a(final String s) {
        if (this.c) {
            return;
        }
        this.c = true;
        this.e.a((cj)null);
        this.e.a(new cf("Connection lost", s));
    }
    
    public void a(final fd fd) {
        if (this.c) {
            return;
        }
        this.d.a(fd);
    }
    
    @Override
    public void a(final bi bi) {
        final dr dr = (dr)this.f.b(bi.a);
        fv g = (fv)this.f.b(bi.b);
        if (g == null) {
            g = this.e.g;
        }
        if (dr != null) {
            this.f.a(dr, "random.pop", 0.2f, ((this.b.nextFloat() - this.b.nextFloat()) * 0.7f + 1.0f) * 2.0f);
            this.e.h.a(new ca(this.e.e, dr, g, -0.5f));
            this.f.c(bi.a);
        }
    }
    
    @Override
    public void a(final dt dt) {
        final jx b = this.f.b(dt.a);
        if (b == null) {
            return;
        }
        final dg dg = (dg)b;
        final int b2 = dt.b;
        if (b2 == 0) {
            dg.b.a[dg.b.d] = null;
        }
        else {
            dg.b.a[dg.b.d] = new eo(b2);
        }
    }
    
    @Override
    public void a(final hz hz) {
        this.e.u.a(hz.a);
    }
    
    @Override
    public void a(final gv gv) {
        final jx b = this.f.b(gv.a);
        if (b == null) {
            return;
        }
        ((dg)b).u();
    }
    
    @Override
    public void a(final ks ks) {
        this.e.g.b.a(new eo(ks.a, ks.b, ks.c));
    }
    
    @Override
    public void a(final gk gk) {
        if (gk.a.equals("-")) {
            this.a((fd)new hf(this.e.i.b, "Password", 14));
        }
        else {
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + this.e.i.b + "&sessionId=" + this.e.i.c + "&serverId=" + gk.a).openStream()));
                final String line = bufferedReader.readLine();
                bufferedReader.close();
                if (line.equalsIgnoreCase("ok")) {
                    this.a((fd)new hf(this.e.i.b, "Password", 14));
                }
                else {
                    this.d.a("Failed to login: " + line);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
                this.d.a("Internal client error: " + ex.toString());
            }
        }
    }
    
    public void b() {
        this.c = true;
        this.d.a("Closed");
    }
}
