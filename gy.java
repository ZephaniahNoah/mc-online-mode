import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Random;
import net.minecraft.client.Minecraft;

// 
// Decompiled by Procyon v0.5.36
// 

public class gy extends lb
{
    private boolean c;
    private ii d;
    public String a;
    private Minecraft e;
    private gs f;
    private boolean g;
    Random b;
    
    public gy(final Minecraft e, final String host, final int port) {
        this.c = false;
        this.g = false;
        this.b = new Random();
        this.e = e;
        try {
            this.d = new ii(new Socket(InetAddress.getByName(host), port), "Client", this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void a() {
        if (this.c) {
            return;
        }
        this.d.a();
    }
    
    @Override
    public void a(final hp hp) {
        this.e.b = new ni(this.e, this);
        this.f = new gs(this);
        this.f.y = true;
        this.e.a(this.f);
        this.e.a(new dg(this));
    }
    
    @Override
    public void a(final ha ha) {
        final dx dx = new dx(this.f, ha.b / 32.0, ha.c / 32.0, ha.d / 32.0, new ev(ha.h, ha.i));
        dx.an = ha.e / 128.0;
        dx.ao = ha.f / 128.0;
        dx.ap = ha.g / 128.0;
        dx.bd = ha.b;
        dx.be = ha.c;
        dx.bf = ha.d;
        this.f.a(ha.a, dx);
    }
    
    @Override
    public void a(final kj kj) {
        final double n = kj.b / 32.0;
        final double n2 = kj.c / 32.0;
        final double n3 = kj.d / 32.0;
        kh kh = null;
        if (kj.e == 10) {
            kh = new ob(this.f, n, n2, n3, 0);
        }
        if (kj.e == 11) {
            kh = new ob(this.f, n, n2, n3, 1);
        }
        if (kj.e == 12) {
            kh = new ob(this.f, n, n2, n3, 2);
        }
        if (kj.e == 1) {
            kh = new dc(this.f, n, n2, n3);
        }
        if (kh != null) {
            kh.bd = kj.b;
            kh.be = kj.c;
            kh.bf = kj.d;
            kh.aq = 0.0f;
            kh.ar = 0.0f;
            kh.ab = kj.a;
            this.f.a(kj.a, kh);
        }
    }
    
    @Override
    public void a(final gp gp) {
        final double n = gp.c / 32.0;
        final double n2 = gp.d / 32.0;
        final double n3 = gp.e / 32.0;
        final float n4 = gp.f * 360 / 256.0f;
        final float n5 = gp.g * 360 / 256.0f;
        final ns ns = new ns(this.e.e, gp.b);
        ns.bd = gp.c;
        ns.be = gp.d;
        ns.bf = gp.e;
        final int h = gp.h;
        if (h == 0) {
            ns.b.a[ns.b.d] = null;
        }
        else {
            ns.b.a[ns.b.d] = new ev(h);
        }
        ns.b(n, n2, n3, n4, n5);
        this.f.a(gp.a, ns);
    }
    
    @Override
    public void a(final jl jl) {
        final kh b = this.f.b(jl.a);
        if (b == null) {
            return;
        }
        b.bd = jl.b;
        b.be = jl.c;
        b.bf = jl.d;
        b.a(b.bd / 32.0, b.be / 32.0, b.bf / 32.0, jl.e * 360 / 256.0f, jl.f * 360 / 256.0f, 3);
    }
    
    @Override
    public void a(final lq lq) {
        final kh b = this.f.b(lq.a);
        if (b == null) {
            return;
        }
        final kh kh = b;
        kh.bd += lq.b;
        final kh kh2 = b;
        kh2.be += lq.c;
        final kh kh3 = b;
        kh3.bf += lq.d;
        b.a(b.bd / 32.0, b.be / 32.0, b.bf / 32.0, lq.g ? (lq.e * 360 / 256.0f) : b.aq, lq.g ? (lq.f * 360 / 256.0f) : b.ar, 3);
    }
    
    @Override
    public void a(final ju ju) {
        this.f.c(ju.a);
    }
    
    @Override
    public void a(final eh eh) {
        final bi g = this.e.g;
        double n = g.ak;
        double n2 = g.al;
        double n3 = g.am;
        float n4 = g.aq;
        float n5 = g.ar;
        if (eh.h) {
            n = eh.a;
            n2 = eh.b;
            n3 = eh.c;
        }
        if (eh.i) {
            n4 = eh.e;
            n5 = eh.f;
        }
        g.aL = 0.0f;
        final bi bi = g;
        final bi bi2 = g;
        final bi bi3 = g;
        final double an = 0.0;
        bi3.ap = an;
        bi2.ao = an;
        bi.an = an;
        g.b(n, n2, n3, n4, n5);
        eh.a = g.ak;
        eh.b = g.au.b;
        eh.c = g.am;
        eh.d = g.al;
        this.d.a(eh);
        if (!this.g) {
            this.e.g.ah = this.e.g.ak;
            this.e.g.ai = this.e.g.al;
            this.e.g.aj = this.e.g.am;
            this.g = true;
            this.e.a((bh)null);
        }
    }
    
    @Override
    public void a(final ka ka) {
        this.f.a(ka.a, ka.b, ka.c);
    }
    
    @Override
    public void a(final mz mz) {
        final ga b = this.f.b(mz.a, mz.b);
        final int n = mz.a * 16;
        final int n2 = mz.b * 16;
        for (int i = 0; i < mz.f; ++i) {
            final short n3 = mz.c[i];
            final int n4 = mz.d[i] & 0xFF;
            final byte b2 = mz.e[i];
            final int n5 = n3 >> 12 & 0xF;
            final int n6 = n3 >> 8 & 0xF;
            final int n7 = n3 & 0xFF;
            b.a(n5, n7, n6, n4, b2);
            this.f.c(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
            this.f.b(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
        }
    }
    
    @Override
    public void a(final bz bz) {
        this.f.c(bz.a, bz.b, bz.c, bz.a + bz.d - 1, bz.b + bz.e - 1, bz.c + bz.f - 1);
        this.f.a(bz.a, bz.b, bz.c, bz.d, bz.e, bz.f, bz.g);
    }
    
    @Override
    public void a(final li li) {
        this.f.c(li.a, li.b, li.c, li.d, li.e);
    }
    
    @Override
    public void a(final og og) {
        this.d.a("Got kicked");
        this.c = true;
        this.e.a((cn)null);
        this.e.a(new cj("Disconnected by server", og.a));
    }
    
    @Override
    public void a(final String s) {
        if (this.c) {
            return;
        }
        this.c = true;
        this.e.a((cn)null);
        this.e.a(new cj("Connection lost", s));
    }
    
    public void a(final fn fn) {
        if (this.c) {
            return;
        }
        this.d.a(fn);
    }
    
    @Override
    public void a(final bm bm) {
        final dx dx = (dx)this.f.b(bm.a);
        ge g = (ge)this.f.b(bm.b);
        if (g == null) {
            g = this.e.g;
        }
        if (dx != null) {
            this.f.a(dx, "random.pop", 0.2f, ((this.b.nextFloat() - this.b.nextFloat()) * 0.7f + 1.0f) * 2.0f);
            this.e.h.a(new cd(this.e.e, dx, g, -0.5f));
            this.f.c(bm.a);
        }
    }
    
    @Override
    public void a(final dz dz) {
        final kh b = this.f.b(dz.a);
        if (b == null) {
            return;
        }
        final dm dm = (dm)b;
        final int b2 = dz.b;
        if (b2 == 0) {
            dm.b.a[dm.b.d] = null;
        }
        else {
            dm.b.a[dm.b.d] = new ev(b2);
        }
    }
    
    @Override
    public void a(final ij ij) {
        this.e.u.a(ij.a);
    }
    
    @Override
    public void a(final hf hf) {
        final kh b = this.f.b(hf.a);
        if (b == null) {
            return;
        }
        ((dm)b).v();
    }
    
    @Override
    public void a(final ld ld) {
        this.e.g.b.a(new ev(ld.a, ld.b, ld.c));
    }
    
    @Override
    public void a(final gt gt) {
        if (gt.a.equals("-")) {
            this.a((fn)new hp(this.e.i.b, "Password", 2));
        }
        else {
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + this.e.i.b + "&sessionId=" + this.e.i.c + "&serverId=" + gt.a).openStream()));
                final String line = bufferedReader.readLine();
                bufferedReader.close();
                if (line.equalsIgnoreCase("ok")) {
                    this.a((fn)new hp(this.e.i.b, "Password", 2));
                }
                else {
                    this.d.a("Failed to login: " + line);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
                this.d.a("Internal client error: " + ex.toString());
            }
        }
    }
    
    public void b() {
        this.c = true;
        this.d.a("Closed");
    }
    
    @Override
    public void a(final ez ez) {
        final double n = ez.c / 32.0;
        final double n2 = ez.d / 32.0;
        final double n3 = ez.e / 32.0;
        final float n4 = ez.f * 360 / 256.0f;
        final float n5 = ez.g * 360 / 256.0f;
        final ge ge = (ge)ew.a(ez.b, this.e.e);
        ge.bd = ez.c;
        ge.be = ez.d;
        ge.bf = ez.e;
        ge.b(n, n2, n3, n4, n5);
        ge.B = true;
        this.f.a(ez.a, ge);
    }
    
    @Override
    public void a(final du du) {
        this.e.e.a(du.a);
    }
    
    @Override
    public void a(final m m) {
        final bi g = this.e.g;
        if (m.a == -1) {
            g.b.a = m.b;
        }
        if (m.a == -2) {
            g.b.c = m.b;
        }
        if (m.a == -3) {
            g.b.b = m.b;
        }
    }
    
    @Override
    public void a(final nx nx) {
        final ic b = this.f.b(nx.a, nx.b, nx.c);
        if (b != null) {
            b.a(nx.e);
            this.f.b(nx.a, nx.b, nx.c, nx.a, nx.b, nx.c);
        }
    }
    
    @Override
    public void a(final ji ji) {
        this.f.o = ji.a;
        this.f.p = ji.b;
        this.f.q = ji.c;
    }
}
