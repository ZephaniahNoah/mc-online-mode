import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Random;
import net.minecraft.client.Minecraft;

// 
// Decompiled by Procyon v0.5.36
// 

public class ht extends mc
{
    private boolean c;
    private jf d;
    public String a;
    private Minecraft e;
    private hn f;
    private boolean g;
    Random b;
    
    public ht(final Minecraft e, final String host, final int port) {
        this.c = false;
        this.g = false;
        this.b = new Random();
        this.e = e;
        try {
            this.d = new jf(new Socket(InetAddress.getByName(host), port), "Client", this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void a() {
        if (this.c) {
            return;
        }
        this.d.a();
    }
    
    @Override
    public void a(final il il) {
        this.e.b = new ot(this.e, this);
        this.f = new hn(this, il.d, il.e);
        this.f.A = true;
        this.e.a(this.f);
        this.e.a(new dp(this));
    }
    
    @Override
    public void a(final hv hv) {
        final ei ei = new ei(this.f, hv.b / 32.0, hv.c / 32.0, hv.d / 32.0, new fi(hv.h, hv.i));
        ei.as = hv.e / 128.0;
        ei.at = hv.f / 128.0;
        ei.au = hv.g / 128.0;
        ei.bj = hv.b;
        ei.bk = hv.c;
        ei.bl = hv.d;
        this.f.a(hv.a, ei);
    }
    
    @Override
    public void a(final ln ln) {
        final double n = ln.b / 32.0;
        final double n2 = ln.c / 32.0;
        final double n3 = ln.d / 32.0;
        lk lk = null;
        if (ln.e == 10) {
            lk = new pq(this.f, n, n2, n3, 0);
        }
        if (ln.e == 11) {
            lk = new pq(this.f, n, n2, n3, 1);
        }
        if (ln.e == 12) {
            lk = new pq(this.f, n, n2, n3, 2);
        }
        if (ln.e == 1) {
            lk = new dk(this.f, n, n2, n3);
        }
        if (lk != null) {
            lk.bj = ln.b;
            lk.bk = ln.c;
            lk.bl = ln.d;
            lk.av = 0.0f;
            lk.aw = 0.0f;
            lk.ag = ln.a;
            this.f.a(ln.a, lk);
        }
    }
    
    @Override
    public void a(final hk hk) {
        final double n = hk.c / 32.0;
        final double n2 = hk.d / 32.0;
        final double n3 = hk.e / 32.0;
        final float n4 = hk.f * 360 / 256.0f;
        final float n5 = hk.g * 360 / 256.0f;
        final pf pf = new pf(this.e.e, hk.b);
        pf.bj = hk.c;
        pf.bk = hk.d;
        pf.bl = hk.e;
        final int h = hk.h;
        if (h == 0) {
            pf.e.a[pf.e.d] = null;
        }
        else {
            pf.e.a[pf.e.d] = new fi(h);
        }
        pf.b(n, n2, n3, n4, n5);
        this.f.a(hk.a, pf);
    }
    
    @Override
    public void a(final kn kn) {
        final lk b = this.f.b(kn.a);
        if (b == null) {
            return;
        }
        b.bj = kn.b;
        b.bk = kn.c;
        b.bl = kn.d;
        b.a(b.bj / 32.0, b.bk / 32.0 + 0.015625, b.bl / 32.0, kn.e * 360 / 256.0f, kn.f * 360 / 256.0f, 3);
    }
    
    @Override
    public void a(final mv mv) {
        final lk b = this.f.b(mv.a);
        if (b == null) {
            return;
        }
        final lk lk = b;
        lk.bj += mv.b;
        final lk lk2 = b;
        lk2.bk += mv.c;
        final lk lk3 = b;
        lk3.bl += mv.d;
        b.a(b.bj / 32.0, b.bk / 32.0 + 0.015625, b.bl / 32.0, mv.g ? (mv.e * 360 / 256.0f) : b.av, mv.g ? (mv.f * 360 / 256.0f) : b.aw, 3);
    }
    
    @Override
    public void a(final kw kw) {
        this.f.c(kw.a);
    }
    
    @Override
    public void a(final eu eu) {
        final bo g = this.e.g;
        double n = g.ap;
        double n2 = g.aq;
        double n3 = g.ar;
        float n4 = g.av;
        float n5 = g.aw;
        if (eu.h) {
            n = eu.a;
            n2 = eu.b;
            n3 = eu.c;
        }
        if (eu.i) {
            n4 = eu.e;
            n5 = eu.f;
        }
        g.aQ = 0.0f;
        final bo bo = g;
        final bo bo2 = g;
        final bo bo3 = g;
        final double as = 0.0;
        bo3.au = as;
        bo2.at = as;
        bo.as = as;
        g.b(n, n2, n3, n4, n5);
        eu.a = g.ap;
        eu.b = g.az.b;
        eu.c = g.ar;
        eu.d = g.aq;
        this.d.a(eu);
        if (!this.g) {
            this.e.g.am = this.e.g.ap;
            this.e.g.an = this.e.g.aq;
            this.e.g.ao = this.e.g.ar;
            this.g = true;
            this.e.a((bn)null);
        }
    }
    
    @Override
    public void a(final ld ld) {
        this.f.a(ld.a, ld.b, ld.c);
    }
    
    @Override
    public void a(final oi oi) {
        final gs c = this.f.c(oi.a, oi.b);
        final int n = oi.a * 16;
        final int n2 = oi.b * 16;
        for (int i = 0; i < oi.f; ++i) {
            final short n3 = oi.c[i];
            final int n4 = oi.d[i] & 0xFF;
            final byte b = oi.e[i];
            final int n5 = n3 >> 12 & 0xF;
            final int n6 = n3 >> 8 & 0xF;
            final int n7 = n3 & 0xFF;
            c.a(n5, n7, n6, n4, b);
            this.f.c(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
            this.f.b(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
        }
    }
    
    @Override
    public void a(final cf cf) {
        this.f.c(cf.a, cf.b, cf.c, cf.a + cf.d - 1, cf.b + cf.e - 1, cf.c + cf.f - 1);
        this.f.a(cf.a, cf.b, cf.c, cf.d, cf.e, cf.f, cf.g);
    }
    
    @Override
    public void a(final ml ml) {
        this.f.c(ml.a, ml.b, ml.c, ml.d, ml.e);
    }
    
    @Override
    public void a(final pv pv) {
        this.d.a("Got kicked");
        this.c = true;
        this.e.a((cu)null);
        this.e.a(new cp("Disconnected by server", pv.a));
    }
    
    @Override
    public void a(final String s) {
        if (this.c) {
            return;
        }
        this.c = true;
        this.e.a((cu)null);
        this.e.a(new cp("Connection lost", s));
    }
    
    public void a(final gc gc) {
        if (this.c) {
            return;
        }
        this.d.a(gc);
    }
    
    @Override
    public void a(final bs bs) {
        final ei ei = (ei)this.f.b(bs.a);
        gx g = (gx)this.f.b(bs.b);
        if (g == null) {
            g = this.e.g;
        }
        if (ei != null) {
            this.f.a(ei, "random.pop", 0.2f, ((this.b.nextFloat() - this.b.nextFloat()) * 0.7f + 1.0f) * 2.0f);
            this.e.h.a(new cj(this.e.e, ei, g, -0.5f));
            this.f.c(bs.a);
        }
    }
    
    @Override
    public void a(final ek ek) {
        final lk b = this.f.b(ek.a);
        if (b == null) {
            return;
        }
        final dv dv = (dv)b;
        final int b2 = ek.b;
        if (b2 == 0) {
            dv.e.a[dv.e.d] = null;
        }
        else {
            dv.e.a[dv.e.d] = new fi(b2);
        }
    }
    
    @Override
    public void a(final jg jg) {
        this.e.u.a(jg.a);
    }
    
    @Override
    public void a(final ia ia) {
        final lk b = this.f.b(ia.a);
        if (b == null) {
            return;
        }
        ((dv)b).y();
    }
    
    @Override
    public void a(final mh mh) {
        this.e.g.e.a(new fi(mh.a, mh.b, mh.c));
    }
    
    @Override
    public void a(final ho ho) {
        if (ho.a.equals("-")) {
            this.a((gc)new il(this.e.i.b, "Password", 3));
        }
        else {
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + this.e.i.b + "&sessionId=" + this.e.i.c + "&serverId=" + ho.a).openStream()));
                final String line = bufferedReader.readLine();
                bufferedReader.close();
                if (line.equalsIgnoreCase("ok")) {
                    this.a((gc)new il(this.e.i.b, "Password", 3));
                }
                else {
                    this.d.a("Failed to login: " + line);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
                this.d.a("Internal client error: " + ex.toString());
            }
        }
    }
    
    public void b() {
        this.c = true;
        this.d.a("Closed");
    }
    
    @Override
    public void a(final fn fn) {
        final double n = fn.c / 32.0;
        final double n2 = fn.d / 32.0;
        final double n3 = fn.e / 32.0;
        final float n4 = fn.f * 360 / 256.0f;
        final float n5 = fn.g * 360 / 256.0f;
        final gx gx = (gx)fj.a(fn.b, this.e.e);
        gx.bj = fn.c;
        gx.bk = fn.d;
        gx.bl = fn.e;
        gx.b(n, n2, n3, n4, n5);
        gx.G = true;
        this.f.a(fn.a, gx);
    }
    
    @Override
    public void a(final ee ee) {
        this.e.e.a(ee.a);
    }
    
    @Override
    public void a(final n n) {
        final bo g = this.e.g;
        if (n.a == -1) {
            g.e.a = n.b;
        }
        if (n.a == -2) {
            g.e.c = n.b;
        }
        if (n.a == -3) {
            g.e.b = n.b;
        }
    }
    
    @Override
    public void a(final pl pl) {
        if (pl.e.e("x") != pl.a) {
            return;
        }
        if (pl.e.e("y") != pl.b) {
            return;
        }
        if (pl.e.e("z") != pl.c) {
            return;
        }
        final iz b = this.f.b(pl.a, pl.b, pl.c);
        if (b != null) {
            try {
                b.a(pl.e);
            }
            catch (Exception ex) {}
            this.f.b(pl.a, pl.b, pl.c, pl.a, pl.b, pl.c);
        }
    }
    
    @Override
    public void a(final kk kk) {
        this.f.n = kk.a;
        this.f.o = kk.b;
        this.f.p = kk.c;
    }
}
