import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.Socket;
import java.net.InetAddress;
import java.util.Random;
import net.minecraft.client.Minecraft;

// 
// Decompiled by Procyon v0.5.36
// 

public class gx extends kz
{
    private boolean c;
    private ih d;
    public String a;
    private Minecraft e;
    private gr f;
    private boolean g;
    Random b;
    
    public gx(final Minecraft e, final String host, final int port) {
        this.c = false;
        this.g = false;
        this.b = new Random();
        this.e = e;
        try {
            this.d = new ih(new Socket(InetAddress.getByName(host), port), "Client", this);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void a() {
        if (this.c) {
            return;
        }
        this.d.a();
    }
    
    @Override
    public void a(final ho ho) {
        this.e.b = new ng(this.e, this);
        this.f = new gr(this);
        this.f.x = true;
        this.e.a(this.f);
        this.e.a(new df(this));
    }
    
    @Override
    public void a(final gz gz) {
        final dw dw = new dw(this.f, gz.b / 32.0, gz.c / 32.0, gz.d / 32.0, new eu(gz.h, gz.i));
        dw.am = gz.e / 128.0;
        dw.an = gz.f / 128.0;
        dw.ao = gz.g / 128.0;
        dw.bc = gz.b;
        dw.bd = gz.c;
        dw.be = gz.d;
        this.f.a(gz.a, dw);
    }
    
    @Override
    public void a(final kh kh) {
        final double n = kh.b / 32.0;
        final double n2 = kh.c / 32.0;
        final double n3 = kh.d / 32.0;
        kf kf = null;
        if (kh.e == 10) {
            kf = new ny(this.f, n, n2, n3, 0);
        }
        if (kh.e == 11) {
            kf = new ny(this.f, n, n2, n3, 1);
        }
        if (kh.e == 12) {
            kf = new ny(this.f, n, n2, n3, 2);
        }
        if (kh.e == 1) {
            kf = new db(this.f, n, n2, n3);
        }
        if (kf != null) {
            kf.bc = kh.b;
            kf.bd = kh.c;
            kf.be = kh.d;
            kf.ap = 0.0f;
            kf.aq = 0.0f;
            kf.ab = kh.a;
            this.f.a(kh.a, kf);
        }
    }
    
    @Override
    public void a(final go go) {
        final double n = go.c / 32.0;
        final double n2 = go.d / 32.0;
        final double n3 = go.e / 32.0;
        final float n4 = go.f * 360 / 256.0f;
        final float n5 = go.g * 360 / 256.0f;
        final nq nq = new nq(this.e.e, go.b);
        nq.bc = go.c;
        nq.bd = go.d;
        nq.be = go.e;
        final int h = go.h;
        if (h == 0) {
            nq.b.a[nq.b.d] = null;
        }
        else {
            nq.b.a[nq.b.d] = new eu(h);
        }
        nq.b(n, n2, n3, n4, n5);
        this.f.a(go.a, nq);
    }
    
    @Override
    public void a(final jj jj) {
        final kf b = this.f.b(jj.a);
        if (b == null) {
            return;
        }
        b.bc = jj.b;
        b.bd = jj.c;
        b.be = jj.d;
        b.a(b.bc / 32.0, b.bd / 32.0, b.be / 32.0, jj.e * 360 / 256.0f, jj.f * 360 / 256.0f, 3);
    }
    
    @Override
    public void a(final lo lo) {
        final kf b = this.f.b(lo.a);
        if (b == null) {
            return;
        }
        final kf kf = b;
        kf.bc += lo.b;
        final kf kf2 = b;
        kf2.bd += lo.c;
        final kf kf3 = b;
        kf3.be += lo.d;
        b.a(b.bc / 32.0, b.bd / 32.0, b.be / 32.0, lo.g ? (lo.e * 360 / 256.0f) : b.ap, lo.g ? (lo.f * 360 / 256.0f) : b.aq, 3);
    }
    
    @Override
    public void a(final js js) {
        this.f.c(js.a);
    }
    
    @Override
    public void a(final eg eg) {
        final bh g = this.e.g;
        double n = g.aj;
        double n2 = g.ak;
        double n3 = g.al;
        float n4 = g.ap;
        float n5 = g.aq;
        if (eg.h) {
            n = eg.a;
            n2 = eg.b;
            n3 = eg.c;
        }
        if (eg.i) {
            n4 = eg.e;
            n5 = eg.f;
        }
        g.aK = 0.0f;
        final bh bh = g;
        final bh bh2 = g;
        final bh bh3 = g;
        final double am = 0.0;
        bh3.ao = am;
        bh2.an = am;
        bh.am = am;
        g.b(n, n2, n3, n4, n5);
        eg.a = g.aj;
        eg.b = g.at.b;
        eg.c = g.al;
        eg.d = g.ak;
        this.d.a(eg);
        if (!this.g) {
            this.e.g.ag = this.e.g.aj;
            this.e.g.ah = this.e.g.ak;
            this.e.g.ai = this.e.g.al;
            this.g = true;
            this.e.a((bg)null);
        }
    }
    
    @Override
    public void a(final jy jy) {
        this.f.a(jy.a, jy.b, jy.c);
    }
    
    @Override
    public void a(final mx mx) {
        final fz b = this.f.b(mx.a, mx.b);
        final int n = mx.a * 16;
        final int n2 = mx.b * 16;
        for (int i = 0; i < mx.f; ++i) {
            final short n3 = mx.c[i];
            final int n4 = mx.d[i] & 0xFF;
            final byte b2 = mx.e[i];
            final int n5 = n3 >> 12 & 0xF;
            final int n6 = n3 >> 8 & 0xF;
            final int n7 = n3 & 0xFF;
            b.a(n5, n7, n6, n4, b2);
            this.f.c(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
            this.f.b(n5 + n, n7, n6 + n2, n5 + n, n7, n6 + n2);
        }
    }
    
    @Override
    public void a(final by by) {
        this.f.c(by.a, by.b, by.c, by.a + by.d - 1, by.b + by.e - 1, by.c + by.f - 1);
        this.f.a(by.a, by.b, by.c, by.d, by.e, by.f, by.g);
    }
    
    @Override
    public void a(final lg lg) {
        this.f.c(lg.a, lg.b, lg.c, lg.d, lg.e);
    }
    
    @Override
    public void a(final od od) {
        this.d.a("Got kicked");
        this.c = true;
        this.e.a((cm)null);
        this.e.a(new ci("Disconnected by server", od.a));
    }
    
    @Override
    public void a(final String s) {
        if (this.c) {
            return;
        }
        this.c = true;
        this.e.a((cm)null);
        this.e.a(new ci("Connection lost", s));
    }
    
    public void a(final fm fm) {
        if (this.c) {
            return;
        }
        this.d.a(fm);
    }
    
    @Override
    public void a(final bl bl) {
        final dw dw = (dw)this.f.b(bl.a);
        gd g = (gd)this.f.b(bl.b);
        if (g == null) {
            g = this.e.g;
        }
        if (dw != null) {
            this.f.a(dw, "random.pop", 0.2f, ((this.b.nextFloat() - this.b.nextFloat()) * 0.7f + 1.0f) * 2.0f);
            this.e.h.a(new cc(this.e.e, dw, g, -0.5f));
            this.f.c(bl.a);
        }
    }
    
    @Override
    public void a(final dy dy) {
        final kf b = this.f.b(dy.a);
        if (b == null) {
            return;
        }
        final dl dl = (dl)b;
        final int b2 = dy.b;
        if (b2 == 0) {
            dl.b.a[dl.b.d] = null;
        }
        else {
            dl.b.a[dl.b.d] = new eu(b2);
        }
    }
    
    @Override
    public void a(final ii ii) {
        this.e.u.a(ii.a);
    }
    
    @Override
    public void a(final he he) {
        final kf b = this.f.b(he.a);
        if (b == null) {
            return;
        }
        ((dl)b).v();
    }
    
    @Override
    public void a(final lb lb) {
        this.e.g.b.a(new eu(lb.a, lb.b, lb.c));
    }
    
    @Override
    public void a(final gs gs) {
        if (gs.a.equals("-")) {
            this.a((fm)new ho(this.e.i.b, "Password", 1));
        }
        else {
            try {
                final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL("http://session.minecraft.net/game/joinserver.jsp?user=" + this.e.i.b + "&sessionId=" + this.e.i.c + "&serverId=" + gs.a).openStream()));
                final String line = bufferedReader.readLine();
                bufferedReader.close();
                if (line.equalsIgnoreCase("ok")) {
                    this.a((fm)new ho(this.e.i.b, "Password", 1));
                }
                else {
                    this.d.a("Failed to login: " + line);
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
                this.d.a("Internal client error: " + ex.toString());
            }
        }
    }
    
    public void b() {
        this.c = true;
        this.d.a("Closed");
    }
    
    @Override
    public void a(final ey ey) {
        final double n = ey.c / 32.0;
        final double n2 = ey.d / 32.0;
        final double n3 = ey.e / 32.0;
        final float n4 = ey.f * 360 / 256.0f;
        final float n5 = ey.g * 360 / 256.0f;
        final gd gd = (gd)ev.a(ey.b, this.e.e);
        gd.bc = ey.c;
        gd.bd = ey.d;
        gd.be = ey.e;
        gd.b(n, n2, n3, n4, n5);
        gd.B = true;
        this.f.a(ey.a, gd);
    }
    
    @Override
    public void a(final dt dt) {
        this.e.e.a(dt.a);
    }
}
